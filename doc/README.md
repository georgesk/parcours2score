Parcours2Score, mode d'emploi
=============================

Cette application a été développée initialement pour traiter les
fichiers de suivi du parcours d'un groupe d'élèves, quand ceux-ci se
connectent au service web d'Algoréa, à condition qu'ils soient
inscrits dans un groupe de travail.

Dans un deuxième temps, l'application a été étendue pour prendre en
compte les mêmes parcours d'élèves, mais gérés par la 
**plateforme du Castor**.

Mode d'emploi pour les Castors Informatiques
============================================

Par la suite, nous supposerons que l'enseignant est inscrit comme
**coordinateur**, pour son établissement scolaire, sur la 
**plateforme du Castor**, et qu'un groupe de travail a été créé ;
dans ce document, le groupe de travail se nommera par exemple
`2d2-p1`

Récupération des données des élèves (Castors Informatiques)
-----------------------------------------------------------

Tout d'abord, l'enseignant s'identifie dans le service web
https://coordinateur.castor-informatique.fr/ ; ensuite il suffit

 1. d'activer l'onglet **Groupes** pour voir la liste de tous les groupes
    gérés dans l'établissement.
 2. Un clic sur la bonne ligne, celle du groupe qu'on veut examiner, et
    on voit (sur fond jaune) que le groupe est sélectionné.
 3. Il suffit alors de cliquer sur le bouton 
    **Afficher les résultats détaillés** pour accéder à un
	tableau de scores.
	
Voyez ci-dessous les trois étapes qui permettent d'accéder au tableau des
scores.

![Accès au tableau des scores, en trois clics](snap10.png)
	
Dans ce tableau, chaque item de l'épreuve
possède un score entre 0 et 40, et une colonne donne le total des scores.

Quand la page des scores est affichée, il suffit d'enregistrer le code
de cette page dans un fichier ; par exemple, à partir du menu du
navigateur, par **Fichier -> Enregistrer sous ...**, et enregistrement 
dans un fichier.

### Attention
Le nom du fichier doit se terminer par `.html` : soyez attentif à ce
détail. Par exemple, le nom du fichier pourrait être `2d2-1-data.html` ;
c'est cet exemple qui est utilisé ci-dessous.

Lancement de l'application
--------------------------

Le cas expliqué ici suppose que l'ordinateur est sous système d'exploitation
Linux, que Python3 est installé, avec les modules qui permettent d'animer
une application PyQt5.

On peut vérifier que l'autorisation d'exécution est donnée au fichier
`parcours2score.py`.

Supposons que les deux fichiers `parcours2score.py` et 
`2d2-1-data.html` soient dans le dossier de travail.

Alors, on peut lancer la commande depuis un terminal :
`parcours2score.py 2d2-1-data.html`

Voici une copie d'écran de ce qu'on peut obtenir :

![Après ouverture d'un fichier HTML](snap11.png)

Les actions qui ont un effet sur les notes moyennes des étudiants
-----------------------------------------------------------------

Dans le tableau du parcours, à droite, tous les items dont la valeur
moyenne est nulle sont avec un coefficient zéro : ils ne sont pas
pris en compte dans la notation.

Si par extraordinaire, un de ces items est obligatoire, mais qu'aucun
étudiant ne l'a traité, on peut agir pour que le coefficient ne soit pas
nul.

### Ajustement du coefficient

Le coefficient d'une note sera le même pour tous les élèves du groupe.
Les valeurs possibles sont : 0, 1, 2 et 3 (on peut aussi bien considérer
que ces valeurs sont 0, 1/3, 2/3, 1). Le curseur à gauche de chaque item
permet de préciser ce coefficient.

### Case à cocher « bonus »

Si la case de la colonne **Bonus** est cochée, cela veut dire que le
score obtenu dans l'item correspondant ne sera pris en compte que si
cela peut augmenter la moyenne de l'étudiant.

L'algorithme utilisé est le suivant :

 1. La moyenne (pondérée par les coefficients) de tous les items
    obligatoires est calculée. Sont obligatoires les items dont
	la case Bonus est *non cochée*.
 2. Les items qui ont un case bonus *cochée* sont ensuite ordonnés,
    au cas par cas, selon l'ordre croissant des notes obtenues à ces items.
	Ensuite, pour chacune de ces notes, on se pose la question : 
	« est-elle supérieure à la moyenne ? » ; si c'est le cas, alors
	la note de cet item est prise en compte et une nouvelle moyenne est
	calculée, qui est supérieure à la précédente ; dans le cas contraire,
	la note est ignorée. Puis on passe à l'item suivant qui pourrait
	améliorer la moyenne de l'élève.

### Choix de la note maximale

La note maximale est 20 par défaut ; il est possible de l'ajuster
en modifiant la valeur dans le champ en bas du premier tableau.

### Recalcul de la moyenne

À chaque modification d'un coefficient, d'une case à cocher « Bonus »
ou de la note maximale, le score moyen de chaque étudiant est recalculé et
la moyenne générale qui apparaît tout en bas est mise à jour.

Mode d'emploi pour la plateforme Algoréa
========================================

Un enseignant disposant des prérogatives appropriées peu gérer les
inscriptions à un groupe de travail. Dans ce document le nom du
groupe de travail sera par exemple `JeanBart_2nde07_p1_gk`

Récupération d'un fichier de données
------------------------------------

La première étape se fait dans le service web d'Algoréa ;
l'enseignant doit s'y connecter et s'authentifier. Ensuite, en cliquant
sur son `login` (éventuellement après avoir déplié le menu, trois petits
traits clairs en haut à droite), l'enseignant choisit l'onglet
dénommé « LES GROUPES QUE J'ADMINISTRE ». Dans cet onglet, l'enseignant
clique sur le groupe choisi, par exemple `JeanBart_2nde07_p1_gk`

Une nouvelle série d'onglets apparaît ; l'enseignant choisit l'onglet
dénommé « PROGRESSION ». 

### Choix du parcours

L'enseignant déroule les menu  du parcours et choisit les données
qu'il convient d'exporter. Ici, dans l'exemple ci-dessous, ce sont
les données du parcours **SNT/Données structurées et leur traitement**.

![Choix du parcours](snap1.png)

### Exportation des données

Les étapes sont, dans l'ordre :

 1. Sélection de **SNT** dans le premier menu déroulant
 2. Sélection de **Données structurées et leur traitement** dans le
    deuxième menu déroulant
 3. Clic sur le bouton **Exporter le tableau des scores**
 4. Un bouton **Télécharger** apparaît alors : on clique dessus.
 
Ensuite, soit on enregistre le fichier de données, soit on l'ouvre.
Si on ouvre le fichier dans un tableur, les options d'importation sont :
 * encodage des caractères : Unicode (UTF-8)
 * séparateur : la virgule
 * caractère d'échappement : le guillemet double
 
Dans tous les cas, le fichier doit rester au format CSV, c'est à ce format
que l'application Parcours2Score saura l'utiliser.

Lancement de l'application
--------------------------

Le cas expliqué ici suppose que l'ordinateur est sous système d'exploitation
Linux, que Python3 est installé, avec les modules qui permettent d'animer
une application PyQt5.

On peut vérifier que l'autorisation d'exécution est donnée au fichier
`parcours2score.py`.

Supposons que les deux fichiers `parcours2score.py` et 
`JeanBart_2nde07_p1_gk.csv` soient dans le dossier de travail.

Alors, on peut lancer la commande depuis un terminal :
`parcours2score.py JeanBart_2nde07_p1_gk.csv`

Voici une copie d'écran de ce qu'on obtient, la toute première fois :

![Première fois qu'on ouvre le fichier CSV](snap2.png)

La colonne tout à gauche, intitulée **Nom**, est vide. On y écrit les noms
correspondant aux logins (troisième colonne), à la main. Heureusement,
cette association entre logins et noms sera mémorisée ensuite, dans un
fichier de configuration. Il sera inutile de répéter cette saisie.

Les fois suivantes, on verra plutôt ainsi l'application :

![Ouverture du fichier CSV, les fois suivantes](snap3.png)

Les actions qui ont un effet sur les notes moyennes des étudiants
-----------------------------------------------------------------

Dans le tableau du parcours, à droite, tous les items dont la valeur
moyenne est nulle sont avec un coefficient zéro : ils ne sont pas
pris en compte dans la notation.

Si par extraordinaire, un de ces items est obligatoire, mais qu'aucun
étudiant ne l'a traité, on peut agir pour que le coefficient ne soit pas
nul.

### Ajustement du coefficient

Le coefficient d'une note sera le même pour tous les élèves du groupe.
Les valeurs possibles sont : 0, 1, 2 et 3 (on peut aussi bien considérer
que ces valeurs sont 0, 1/3, 2/3, 1). Le curseur à gauche de chaque item
permet de préciser ce coefficient.

### Case à cocher « bonus »

Si la case de la colonne **Bonus** est cochée, cela veut dire que le
score obtenu dans l'item correspondant ne sera pris en compte que si
cela peut augmenter la moyenne de l'étudiant.

L'algorithme utilisé est le suivant :

 1. La moyenne (pondérée par les coefficients) de tous les items
    obligatoires est calculée. Sont obligatoires les items dont
	la case Bonus est *non cochée*.
 2. Les items qui ont un case bonus *cochée* sont ensuite ordonnés,
    au cas par cas, selon l'ordre croissant des notes obtenues à ces items.
	Ensuite, pour chacune de ces notes, on se pose la question : 
	« est-elle supérieure à la moyenne ? » ; si c'est le cas, alors
	la note de cet item est prise en compte et une nouvelle moyenne est
	calculée, qui est supérieure à la précédente ; dans le cas contraire,
	la note est ignorée. Puis on passe à l'item suivant qui pourrait
	améliorer la moyenne de l'élève.

### Choix de la note maximale

La note maximale est 20 par défaut ; il est possible de l'ajuster
en modifiant la valeur dans le champ en bas du premier tableau.

### Recalcul de la moyenne

À chaque modification d'un coefficient, d'une case à cocher « Bonus »
ou de la note maximale, le score moyen de chaque étudiant est recalculé et
la moyenne générale qui apparaît tout en bas est mise à jour.

