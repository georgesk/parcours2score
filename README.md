DES PARCOURS ALGOREA AUX SCORES DES ÉTUDIANTS
=============================================

Cette application a été développée initialement pour traiter les
fichiers de suivi du parcours d'un groupe d'élèves, quand ceux-ci se
connectent au service web d'Algoréa, à condition qu'ils soient
inscrits dans un groupe de travail.

Dans un deuxième temps, l'application a été étendue pour prendre en
compte les mêmes parcours d'élèves, mais gérés par la **plateforme du Castor**,
connue des nombreux participants au concours **Castor Informatique**
organisée chaque année.

Au jour présent, c'est la **plateforme du Castor** qui est
la plus robuste et peut accueillir un grand nombre de connexions
simultanées, l'autre sert plutôt à titre expérimental.

Nous vous conseillons donc, si cette application vous intéresse, à créer
au plus vite un compte de coordinateur à l'URL
https://coordinateur.castor-informatique.fr/

Ce sont les coordinateurs qui peuvent suivre le travail d'un groupe d'élèves.

Pour plus d'explications, voir le fichier README qui se trouve dans le
sous-dossier `doc/`.

CRÉDITS
-------

>  Auteur : Georges Khaznadar <georgesk@debian.org>

>  Copyright : (c) 2020

>  Licence : GPL-3+

Ce programme est un logiciel libre, vous avez le droit de
l'utiliser, de le copier, de le modifier, et de distribuer
ce programme ou sa forme modifiée. Ceci doit se faire dans
le respect de la licence GPL-3.

Voir le texte complet à https://www.gnu.org/licenses/gpl-3.0.html
