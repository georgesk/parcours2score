#! /usr/bin/python3
"""
Fabrication de scores à partir d'un parcours Algorea, dans le rayon « SNT »

Pour utiliser ce programme, il faut faire partie du groupe d'enseignants,
"Enseignants_SNT", et demander aux élèves de s'inscrire dans des
"Groupes [qu'on] administre". Ensuite, on va gérer les 
"Groupes [qu'on] administre", on choisit un des groupes, et dans ce contexte,
on fait apparaître l'onglet "PROGRESSION", où on peut voir apparaître
les identifiants (logins) des membres du groupe d'élèves. Dans cet
onglet, on peut récupérer un tableau des scores au format CSV, grâce au
bouton "Exporter le tableau des scores".

L'application parcours2score est nourrie par ce fichier CSV et permet
d'attribuer un score à chaque participant, en jouant sur des coefficients ;
ainsi on peut neutraliser les parties du parcours qui sont moins
intéressantes ou significatives du travail de l'élève, que d'autres.
"""

CREDITS = \
"""
Auteur : Georges Khaznadar <georgesk@debian.org>
Copyright : (c) 2020
Licence : GPL-3+

Ce programme est un logiciel libre, vous avez le droit de
l'utiliser, de le copier, de le modifier, et de distribuer
ce programme ou sa forme modifiée. Ceci doit se faire dans
le respect de la licence GPL-3.

Voir le texte complet à https://www.gnu.org/licenses/gpl-3.0.html
"""

import sys, os, re, configparser
from csv import DictReader
from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from layouts import ui_mw
import castor

class DocViewer(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, url):
        self.html = None
        QtWebEngineWidgets.QWebEngineView.__init__(self)
        url = QtCore.QUrl.fromLocalFile(url)
        self.load(url)
        self.resize(1040, 768)
        return

    
class MW(QtWidgets.QMainWindow, ui_mw.Ui_MainWindow):

    coeffChanged = QtCore.pyqtSignal()
    
    def __init__(self, fileName=None, configFilename=None, parent=None):
        """
        Le constructeur
        :param fileName: un fichier CSV du parcours algoréa ou HTML de Castor
        :param configFileName: un fichier de configuration pour des
        données persistantes
        """
        QtWidgets.QMainWindow.__init__(self, parent)
        self.loginKey=None
        if fileName and fileName.endswith(".csv"):
            self.csvFileName = fileName
            self.htmlFile = None
        else:
            self.csvFileName = None
            self.htmlFileName = fileName
        if configFilename:
            self.configFilename = configFilename
        else:
            self.configFilename = os.path.expanduser("~/.config/parcours2score/config.ini")
        self.getConfig()
        self.setupUi(self)
        self.resize(
            int(self.config['DEFAULT']["mw_width"]),
            int(self.config['DEFAULT']["mw_height"]))
        self.max = int(self.config['DEFAULT']["max"])
        self.saveDir = self.config.get("Default","saveDir", fallback = ".")
        self.maxNote.setValue(self.max)
        self.logins = None
        if fileName and fileName.endswith(".csv"):
            self.readCsvFile()
        else:
            self.readHtmlFile()
        self.recalcule()
        self.initCallbacks()
        return

    def closeEvent(self, event):
        self.config['DEFAULT']["mw_width"] = str(self.size().width())
        self.config['DEFAULT']["mw_height"] = str(self.size().height())
        self.config['DEFAULT']["max"] = str(self.maxNote.value())
        self.config['DEFAULT']["saveDir"] = self.saveDir
        if self.logins:
            for row in range(self.tpModel.rowCount()):
                index=self.tpModel.index(row, 0)
                name=self.tpModel.data(index) or ''
                index=self.tpModel.index(row, 2)
                login=self.tpModel.data(index)
                self.config['eleves'][login] = name
                self.config['DEFAULT']["coeff_bonus"] = f"{self.lesCoeffBonus()}"
        with open(self.configFilename, 'w') as configfile:
            self.config.write(configfile)
        return QtWidgets.QMainWindow.closeEvent(self, event)
        

    def getConfig(self):
        if not os.path.isfile(self.configFilename):
            os.makedirs(os.path.dirname(self.configFilename), exist_ok=True)
            config = configparser.ConfigParser()
            config['DEFAULT'] = {
                "mw_width": 800,
                "mw_height": 600,
                "max": 20,
                "coeff_bonus": "[]",
                "saveDir": ".",
            }
            config['eleves'] = {}
            with open(self.configFilename, 'w') as configfile:
                config.write(configfile)
        ### à ce stade, le fichier self.configFilename existe
        self.config = configparser.ConfigParser()
        self.config.read(self.configFilename)
        return

    def nettoieLogin(self, s):
        """
        effaces les parties entre crochets : pas approprié pour un login !
        """
        return re.sub(r"\[.*\]", "", s).strip()

    def lesLogins(self):
        """
        Récupération des logins, avec un nettoyage dans le cas du
        fichier des castors
        """
        return [self.nettoieLogin(data[self.loginKey]) for data in self.dr]
    
    def readCsvFile(self):
        """
        Lecture d'un fichier de parcours algoréa
        Initialise quelques propriétés :
        - self.fieldnames : une liste de descripteurs
        - self.dr : une liste de dictionnaires
        """
        self.loginKey=""
        if self.csvFileName:
            self.setWindowTitle("Score pour un Parcours ALGOREA -- {fichier}".format(fichier=os.path.basename(self.csvFileName)))
            separateur=","
            with open(self.csvFileName) as f:
                l=f.readline()
                if ";" in l:
                    separateur=";"
            self.dr = DictReader(open(self.csvFileName), delimiter=separateur, quotechar='"')
            self.fieldnames = [] + self.dr.fieldnames # fait une copie
            self.dr = list(self.dr)                   # puis lit tout le fichier
            self.logins = self.lesLogins()
        else:
            self.dr = None
            self.data = None
        self.initTableParticipants()
        self.initTableTravaux()
        return

    def readHtmlFile(self):
        """
        Lecture d'un fichier de coordinateur.castor-informatique.fr
        Initialise quelques propriétés :
        - self.fieldnames : une liste de descripteurs
        - self.dr : une liste de dictionnaires
        """
        if self.htmlFileName:
            self.setWindowTitle("Score pour un exercice CASTOR -- {fichier}".format(fichier=os.path.basename(self.htmlFileName)))
            lecteur = castor.CastorLecteurDeResultat(open(self.htmlFileName))
            self.dr = lecteur.dictReader()
            self.fieldnames = [] + self.dr.fieldnames[:-1] # fait une copie
            # remarque : avec Castors Informatiques, le dernier descripteur
            # est un total, donc on ne le prend pas en compte.
            self.dr = list(self.dr)
            for d in self.dr:
                del d["Total"]
            self.loginKey = self.fieldnames[0]
            self.logins = self.lesLogins()
        else:
            self.dr = None
            self.data = None
        self.initTableParticipants()
        self.initTableTravaux()
        return

    def makeReadOnly(self, model, col):
        """
        Place une colonne en lecture seule
        """
        for r in range(model.rowCount()):
            index = model.index(r, col)
            item  = model.itemFromIndex(index)
            item.setFlags(item.flags() & (~ QtCore.Qt.ItemIsEditable))
        return

    def initTableParticipants(self):
        headers = ["Nom", "Score", "Login"]
        if self.logins:
            self.tpModel = QtGui.QStandardItemModel(
                len(self.logins),len(headers),self.tableParticipants)
            self.proxyTpModel =  QtCore.QSortFilterProxyModel()
            self.proxyTpModel.setSourceModel(self.tpModel)
            self.tableParticipants.setModel(self.proxyTpModel)
            row = 0
            for l in self.logins:
                index = self.tpModel.index(row, 2)
                self.tpModel.setData(index, l)
                if l in self.config['eleves']:
                    index = self.tpModel.index(row, 0)
                    self.tpModel.setData(index, self.config['eleves'][l])
                row += 1
            self.tableParticipants.setSortingEnabled(True)
            self.tableParticipants.sortByColumn(0, QtCore.Qt.AscendingOrder)
            self.tableParticipants.resizeColumnToContents(0)
        else:
            self.tpModel = QtGui.QStandardItemModel(
                1,len(headers),self.tableParticipants)
            self.tableParticipants.setModel(self.tpModel)
        for i, h in enumerate(headers):
            self.tpModel.setHeaderData(i, QtCore.Qt.Horizontal, h)
        self.makeReadOnly(self.tpModel,1)
        self.makeReadOnly(self.tpModel,2)
        return

    def toScore(self,data):
        try:
            result=float(data)
        except:
            result=0
        return result

    def initTableTravaux(self):
        headers = ["Coeff", "Bonus", "% moy.", "Activité"]
        if self.logins:
            self.travaux = self.fieldnames[1:]
            self.ttModel = QtGui.QStandardItemModel(
                len(self.travaux),len(headers),self.tableTravaux)
            self.tableTravaux.setModel(self.ttModel)
            row = 0
            for t in self.travaux:
                # mise en place du score moyen
                # il faut le connaître avant de placer le curseur de coeff
                index = self.ttModel.index(row, 2)
                scores = [self.toScore(data[t]) for data in self.dr]
                avg = sum(scores)/len(scores)
                self.ttModel.setData(index, f"{avg:4.1f}")
                # mise en place du curseur de coefficient
                index = self.ttModel.index(row, 0)
                slider=QtWidgets.QSlider(QtCore.Qt.Horizontal)
                slider.setMaximum(3)
                slider.setMinimum(0)
                slider.setTickInterval(1)
                slider.setTickPosition(slider.TicksAbove)
                lesCoeffBonus = eval(self.config["DEFAULT"]["coeff_bonus"])
                if avg == 0.0:
                    slider.setValue(0)
                else:
                    if row < len(lesCoeffBonus):
                        slider.setValue(lesCoeffBonus[row][0])
                    else:
                        slider.setValue(3)
                slider.valueChanged.connect(lambda: self.coeffChanged.emit())
                self.tableTravaux.setIndexWidget(index, slider)
                # mise en place de cases à cocher pour les bonus
                index = self.ttModel.index(row, 1)
                check = QtWidgets.QCheckBox("")
                if row < len(lesCoeffBonus) and lesCoeffBonus[row][1]:
                    check.setCheckState(QtCore.Qt.Checked)
                check.stateChanged.connect(lambda: self.coeffChanged.emit())
                self.tableTravaux.setIndexWidget(index, check)
                # mise en place du nom du travail dans le parcours
                index = self.ttModel.index(row, 3)
                self.ttModel.setData(index, t)
                ########
                row += 1
            ### Réglages de largeur
            self.tableTravaux.setColumnWidth(1, 50)
            self.tableTravaux.setColumnWidth(2, 50)
            self.tableTravaux.resizeColumnToContents(3)
        else:
            self.travaux=[]
            self.ttModel = QtGui.QStandardItemModel(
                1,len(headers),self.tableTravaux)
            self.tableTravaux.setModel(self.ttModel)
        for i, h in enumerate(headers):
            self.ttModel.setHeaderData(i, QtCore.Qt.Horizontal, h)
        self.makeReadOnly(self.ttModel,0)
        self.makeReadOnly(self.ttModel,1)
        return

    def initCallbacks(self):
        self.actionQuitter.triggered.connect(QtWidgets.QApplication.instance().quit)
        self.coeffChanged.connect(self.recalcule)
        self.maxNote.valueChanged.connect(self.recalcule)
        self.actionOuvrir.triggered.connect(self.openFile)
        self.action_propos.triggered.connect(self.Apropos)
        self.actionDocumentation.triggered.connect(self.documentation)
        self.actionEnregistrer.triggered.connect(self.enregistrer)
        return

    def enregistrer(self):
        """
        Enregistre le tableau de gauche au format CSV
        """
        f, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Enregistrer les notes", self.saveDir, "Fichers CSV (*.csv);;Tout type de fichier (*)")
        if f:
            self.saveDir = os.path.dirname(f)
            if not f.endswith(".csv"):
                f+=(".csv")
            with open(f,"w") as savefile:
                savefile.write("nom,note,login\n")
                for j in range(self.tpModel.rowCount()):
                    nom = '"'+(self.tpModel.index(j, 0).data() or "")+'"'
                    note = float(self.tpModel.index(j, 1).data())
                    login = '"'+self.tpModel.index(j, 2).data()+'"'
                    savefile.write(f"{nom},{note},{login}\n")
        return
        
    def documentation (self):
        """
        Affiche la documentation
        """
        readme=os.path.join(
            os.path.dirname(__file__), "doc", "README.html"
        )
        self.doc = DocViewer(os.path.abspath(readme))
        self.doc.show()
        return

    def Apropos(self):
        QtWidgets.QMessageBox.information(self,"À propos", CREDITS)

    def openFile(self):
        f, _ = QtWidgets.QFileDialog.getOpenFileName(caption = "Choisir les notes de parcours", filter = "Fichiers HTML ou CSV (*.html *.csv);;Tous fichiers (*)")
        if f:
            if f.endswith(".csv"):
                self.csvFileName = f
                self.htmlFile = None
                self.readCsvFile()
                self.recalcule()
            else:
                self.csvFileName = None
                self.htmlFile = h
                self.readHtmlFile()
                self.recalcule()
        return

    def lesCoeffBonus(self):
        """
        Fait une liste de doublets (coefficient, bonus)
        """
        return [self.coeffBonus(r) for r in range(self.ttModel.rowCount())]
            
    def recalcule(self):
        """
        Relance un calcul des notes
        """
        if self.csvFileName:
            scoreMax = 100 # Algorea note de 0 à 100
        else:
            scoreMax = 40  # Castor note de 0 à 40
        if not self.logins:
            return
        doublets = self.lesCoeffBonus()

        row = 0
        lesNotes=[]
        for data in self.dr:
            l = self.nettoieLogin(data[self.loginKey]) # le login
            # on prend les scores depuis les colonnes après celle du login
            nb = [self.toScore(data[f]) for f in self.fieldnames[1:]]
            notes =[]
            notes_bonus =[]
            for val, (coeff, bonus) in zip(nb, []+doublets):
                # séparation des notes et des bonus
                if coeff:
                    if bonus:
                        notes_bonus.append((val, coeff))
                    else:
                        notes.append((val, coeff))
            # on range les bonus par valeurs croissantes, pour
            # être sûr de commencer par les plus faibles
            notes_bonus = sorted(notes_bonus, key=lambda nb: nb[0])
            for b in notes_bonus:
                try:
                    moy = sum([n[0]*n[1] for n in notes])/sum([n[1] for n in notes])
                    if b[0] > moy:
                        notes.append(b)
                except ZeroDivisionError:
                    pass
            self.max = int(self.maxNote.text())
            moyenne_bonifiee = 0.0
            try:
                moyenne_bonifiee = sum([n[0]*n[1] for n in notes])/sum([n[1] for n in notes])/scoreMax*self.max
            except ZeroDivisionError:
                pass
            # on inscrit la moyenne bonifiée dans la deuxième colonne
            # du tableau des participants
            index = self.tpModel.index(row, 2)
            assert(self.tpModel.data(index) == l) # vérif : c'est le bon login
            index = self.tpModel.index(row, 1)
            self.tpModel.setData(index, round(10*moyenne_bonifiee)/10)
            lesNotes.append(moyenne_bonifiee)
            row += 1
        moyenne_generale = sum(lesNotes)/len(lesNotes)
        self.moyenneEdit.setText(f"{moyenne_generale:4.1f}")
        return
    
    def coeffBonus(self, row):
        """
        Renvoie un doublet coeff, bonus pour la ligne demandée
        """
        if not self.logins:
            return None, None
        index = self.ttModel.index(row, 0) # coeff
        coeff = self.tableTravaux.indexWidget(index).value()
        index = self.ttModel.index(row, 1) # bonus
        bonus = int(self.tableTravaux.indexWidget(index).checkState())
        return coeff, bonus

        
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    if len(sys.argv) > 1:
        w=MW(sys.argv[1])
    else:
        w=MW()
    w.show()
    sys.exit(app.exec_())
