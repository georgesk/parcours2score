DESTDIR = 
SUBDIRS = layouts doc

all:
	for d in $(SUBDIRS); do \
	  $(MAKE) $@ -C $$d; \
	done
clean:
	find . -name __pycache__ | xargs rm -rf
	find . -name "*~" | xargs rm -f

install:

.PHONY: all clean install
