#! /usr/bin/python3

"""
Traitement de la page HTML de résultats qu'on peut récupérer en tant que
coordonateur à https://coordinateur.castor-informatique.fr/
"""
from bs4 import BeautifulSoup as BS
from xml.dom import minidom
import re

def textData(node):
    """
    Renvoie la donnée texte d'un nœud de xml.dom
    """
    return " ".join(textDataCumul(node,[]))


def textDataCumul(node, cumul):
    """
    fonction récursive récupérant le texte d'un nœud dans un
    accumulateur.
    """
    if node.nodeType ==  node.TEXT_NODE:
        t = node.data.strip()
        if t:
            return cumul + [t]
        else:
            return cumul
    else:
        for child_node in node.childNodes:
            cumul = textDataCumul(child_node, cumul)
        return cumul
    

class DictReader:
    """
    Classe pour un lecteur qui permet de connaître les descripteurs,
    et qui donne à chaque lecture les données d'une ligne de plus dans
    le tableau, sous forme de dictionnaire.
    """
    def __init__(self, descripteurs, lignes):
        """
        Le constructeur
        :param descripteurs: les clés utilisables dans les dictionnaires
        :param lignes: des lignes de table HTML (NodeList)
        """
        self.lignes = lignes
        self.fieldnames = descripteurs
        self._next = 0
        return

    def __iter__(self):
        return self

    def ligne2dico(self, ligne):
        """
        Fait de la ligne courante du tableau un dictionnaire
        """
        tds=ligne.getElementsByTagName("td")
        return dict(zip([]+self.fieldnames, [textData(td) for td in tds]))

    def __next__(self):
        """
        L'itérateur permet de trouver à chaque fois les données suivantes
        """
        if self._next >= len(self.lignes):
            raise (StopIteration)
        else:
            dico=self.ligne2dico(self.lignes[self._next])
            self._next += 1
            return dico
            
class CastorLecteurDeResultat:
    """
    Un lecteur de tableaux de résultats tels qu'ils sont affichés
    par l'outil destiné aux coordinateurs (version de mai 2020).
    Comme le code HTML est syntaxiquement faux, une première passe est
    nécessaire pour soigner certaines apostrophes, puis on utilise
    BeautifulSoup pour « beautifier » le code, qu'on analyse enfin
    à l'aide de xml.dom.minidom.
    """

    def __init__(self, page):
        """
        Le constructeur.
        :param page: du code HTML pas nécessairement très propre
        :type  page: str ou un lecteur de flux
        """
        if type(page) != str:
            page=page.read()
        page=self.encode_apostrophe(page)
        soup = BS(page, 'html.parser')
        self.soup = soup.prettify()
        self.doc = minidom.parseString(self.soup)
        self.table = self.doc.getElementsByTagName("table")[0]
        self.lignes = self.table.getElementsByTagName("tr")
        self.fieldnames = [textData(th) for th in self.lignes[0].getElementsByTagName("th")]
        self.lignes=self.lignes[1:]
        return
    
    def encode_apostrophe(self, s):
        """
        Détecte la présence de texte avec une apostrophe et
        remplace par une entité HTML
        :param s: une chaîne de caractères
        :type  s: str
        """
        return re.sub(r"([A-Za-z])'([A-Za-z])", r"\1&apos;\2", s)

    def dictReader(self):
        """
        Renvoie un lecteur qui permet de connaître les descripteurs,
        et qui donne à chaque lecture les données d'une ligne de plus dans
        le tableau, sous forme de dictionnaire.
        """
        return DictReader(self.fieldnames, self.lignes)

if __name__ == "__main__":
    cldr = CastorLecteurDeResultat(open("data.html"))
    dr = cldr.dictReader()
    for row in dr:
        print(row)
